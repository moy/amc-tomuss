# AMC and TOMUSS

A few utilities and advises for using [Auto Multiple Choice](http://auto-multiple-choice.net/), especially in combination with [TOMUSS](http://perso.univ-lyon1.fr/thierry.excoffier/TOMUSS/home.html).

## Scan

Black & white (no greyscale, sometimes called "text only"), 200 dpi is fine.

Scan and import by batch of ~50 copies, in case something goes wrong.

Scan to a USB key, the file will be too big for email.

## Export results to TOMUSS

### Grades

If you use an anonymization code (i.e. student typed his sheet number, not his student ID on his paper) :

* Type sheet numbers into a TOMUSS column, e.g. `.SheetNumber`

* Export as CSV (print, export to spreadsheet, copy-paste into Libreoffice and save as CSV)

Import CSV from "marking" tab with "Set file" button.

Tab « reports », « choose columns ». Make sure ID is selected (or you won't be able to import the resulting report into TOMUSS)

"Primary key from this list": column name from the CSV, e.g. `.SheetNumber`

"Code name for automatic association": name of the AMCCodeGrid field in the `.tex` file.

Click "automatic" button.

### Export annotated PDFs

#### Configuration (filename, annotations)

Click the "preferences" button in the title bar of AMC, then "Annotation" tab.

Optional: set the verdict to use on the first page of exported PDFs.

Preferences (top right) -> Project tab -> Paper annotation / header
text:

```
%(id) %(nom) %(prénom).
Note : %s/%m (total score: %S/%M)
```

Or, directly in `options.xml`:

```
  <verdict>%(id) %(nom) %(prénom).
Note : %s/%m (total score: %S/%M)</verdict>
```

#### PNG Vs JPG

PNG gives smaller files (~100KB/page) if you used black&white scan,
and it's lossless.

#### Perform the PDF export

Tab "reports", part "annotated papers".

File name model: `(id)#(Nom)-(Prénom)`

Apparently, on the latest version of AMC, `(id)` must be lower case, `(ID)` gives the name of the student instead.

Click "Annotate papers", then make a ZIP:

```
zip exam.zip *.pdf
```

and use « dépôt de fichiers » column type, action « téléverser » in TOMUSS.

Tip: if you want to know who accessed the file, create another column with the same name plus `.log` at the end (e.g. `Exam/Copie` for the file, and `Exam/Copie.log` to know who accessed the file).

